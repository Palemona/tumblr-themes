# Do-Re-Mi

A theme made for Tumblr blogs. Developed by Palemomos from [Themes By Pale (or Palemomos Themes)](https://themesbypale.tumblr.com/). 

## 🌸 Install this theme

### ✨ Install via Theme Garden

1. To install the theme from the Tumblr's Theme Garden, go to [this link](https://www.tumblr.com/theme/41277) and click on **"Install"**. 

2. A dropdown will appear with a list of your main and side blogs. Select the blog you want to use the theme, and click **"Install"** again.

Note: It is recommended to use Theme Garden to install the theme, because when new changes/fixes are added, the theme will be updated automatically.

### ✨ Install with source code


### Install with source code

1. To install the theme using the source code, go to [this link](https://gitlab.com/Palemona/tumblr-themes/-/raw/master/Do-Re-Mi/do-re-mi.html), you'll be redirected to a page containing the source code.

2. Copy ALL the content of the page. Use <kbd>Ctrl</kbd> + <kbd>A</kbd> or <kbd>Command ⌘</kbd> + <kbd>A</kbd> to select all the text, and then copy it (<kbd>Ctrl</kbd> + <kbd>C</kbd> or <kbd>Command ⌘</kbd> + <kbd>C</kbd>).

3. Go to your blog and find the **Edit appereance** option. You'll be redirected you to the customization page.

4. Click on **Edit HTML**. Delete all the current content, and paste the code copied in step 2 <kbd>Ctrl</kbd> + <kbd>V</kbd> or <kbd>Command ⌘</kbd> + <kbd>V</kbd>.

5. Click **Update Preview** and then click **Save**.

--------

## 🌸 Customization Guide

The theme comes with the following options in the customization panel.

### 🔷 Images

- **Custom Background** : Refers to the image that will be used for the page background. To change the behaviour of the background, see *Background Repeat*, *Background Size* and *Background Attachment*.

- **Side Img** : Refers to the image displayed in the side menu (the center of the tap). 

<figure>
    <img width="300" src="imgs/sideimg.png" />
</figure>


- **Popup Img** : Refers to the image that will be shown on the popup on the left side.

<figure>
    <img width="400" src="imgs/popup.png" />
</figure>


### 🔷 Colors

- **Background Color** : Refers to the color that will be used to fill the background in case there is not any background image set.

- **Text Color** : Refers to the color used for all the texts of the theme, except links and the main title.

- **Accent Color** : Refers to the color used for links and on the main title and on the title of the About Popup if the Rainbow Title option is disabled.

- **Accent Color 2**: Refers to the color used in the background of the controls and information of each post.

<figure>
    <img width="300" src="imgs/accentcolors.png" />
    <figcaption>Accent color is dark pink and Accent color 2 is light pink</figcaption>
</figure>

- **Title Shadow Color** : Refers to the color that will be used in the shadow of the main title and on the title of the About Popup. To active this option see *Title Shadow* on Conditionals section.

- **Posts Background** : Refers to the color that will be used for the background of the posts.

- **Tap Background** : Refers to the color that will be used for the background of the side menu.

- **Button n Color** : Refers to the colors of the 8 buttons of the side menu.

<figure>
    <img width="300" src="imgs/tapbuttons.png" />
    <figcaption>All buttons with their corresponding order number.</figcaption>
</figure>


### 🔷 Mandatory options

- **Background Size** : Refers to the possible size of the background image. 

| Value   | Description |
| -------- | ------- |
| auto | The background image is displayed in its original size.  |
| cover | 	Resize the background image to cover the entire container, even if it has to stretch the image or cut a little bit off one of the edges.     |
| contain    | Resize the background image to make sure the image is fully visible.  |

- **Background Repeat** : Refers to the repeating behaviour of the background image.

| Value   | Description |
| -------- | ------- |
| repeat | The background image is repeated both vertically and horizontally.  The last image will be clipped if it does not fit.  |
| repeat-x | 	The background image is repeated only horizontally.     |
| repeat-y    | The background image is repeated only vertically. |
| no-repeat    | The background-image is not repeated. The image will only be shown once. |

- **Background Attachment** : Refers whether the background image scrolls with the rest of the page, or is fixed.

| Value   | Description |
| -------- | ------- |
| fixed | 	The background image will not scroll with the page. |
| scroll | 		The background image will scroll with the page.     |

- **Font Style Posts** : Refers to the font which will be used in all the theme except the blog's title and links. Possible values are listed on image A.

- **Font Style Title** : Refers to the font which will be used only on the blog's main title and on the title of the About Popup. Possible values are listed on image A.

- **Font Style Link** : Refers to the font which will be used only on the links. Possible values are listed on image A.

<figure>
    <img width="300" src="imgs/fonts.png" />
    <figcaption>Image A. List of all possible values for fonts.</figcaption>
</figure>

- **Font Size Posts** : Refers to the general size of the texts of the theme except the description box and the main title and the title of the About Popup. The possible values are *12px*, *14px*, *16px* and *18px*.

- **Description Text Size** : Refers to the size of the text of the description box. The possible values are *12px*, *14px*, *16px* and *18px*.


### 🔷 Conditionals

- **Two Columns** : Refers if the posts will display in two columns if active or in one. Note that posts will be displayed in the following behaviour:

```
1 | 4
2 | 5
3 | 6
```

- **Show About Section** : Refers if the About PopUp will be rendered or not. If active, when the user clicks on the button of the center of the side menu, a pop up will render. To customize this section, check *PopUp Img*, *Tags* and *Links*.

- **Show Window Img** : Refers if the left image of the About PopUp will be rendered or not, If inactive, all the content will just extend to the available width.

<figure>
    <img width="400" src="imgs/aboutnoimg.png" />
    <figcaption>About PopUp without side image.</figcaption>
</figure>

- **Musical Cursor** : Refers if the cursor will be replaced by one with a musical note.

- **Rainbow Title** : Refers if the title will be filled with a rainbow gradient. If active, the colors selected on the *Button n color* section will be used to generate the gradient. If inactive, the title will be filled with the *Accent Color*.

- **Title Shadow** : Refers if a shadow for the main title and the title of the popup will be rendered. The color is the one selected on *Title Shadow Color*.

- **Hide Musical Note** : Refers if the musical note on the center of the side menu will be shown or not. If inactive, the side menu will always show the image selected on *Side Img*.

<figure>
    <img width="300" src="imgs/nomusicalnote.png" />
    <figcaption>Side menu without the musical note button.</figcaption>
</figure>

- **Icons Always Visible** : Refers if the icons of the buttons of the side menu will be visible only when the cursor hovers the button or if the icon will be always visible.

<figure>
    <img width="300" src="imgs/iconsvisible.png" />
    <figcaption>Icons always visible.</figcaption>
</figure>


- **Colored Border** : Refers if the posts and the About popup will use an image border. If active, the border will be color dots, if inactive the border will be gray dashes.

<figure>
    <img width="300" src="imgs/normaldots.png" />
    <figcaption>Colored Border.</figcaption>
</figure>

<figure>
    <img width="300" src="imgs/dashedborder.png" />
    <figcaption>Dashed Border.</figcaption>
</figure>

- **Colored Border Big Dots** : Refers to the size of the dots if the *Colored Border* is active. If this option is active, the dots will be bigger.

<figure>
    <img width="300" src="imgs/coloreddots.png" />
    <figcaption>Border with big dots.</figcaption>
</figure>

- **Show Notes Backdrop** : Refers if the texts for the note count and the date of the posts will render a backdrop.

<figure>
    <img width="300" src="imgs/notesbackdrop.png" />
    <figcaption>Border with big dots.</figcaption>
</figure>

### 🔷 Input Texts

- **Link 1** : Refers to the text that will be shown for the first custom link on the side menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 1 URL** : Refers to the URL for the first custom link on the side menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage. Example: *https://www.youtube.com/watch?v=dQw4w9WgXcQ* .

- **Link 2** : Refers to the text that will be shown for the second custom link on the side menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 2 URL** : Refers to the URL for the second custom link on the side menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage. 

- **Link 3** : Refers to the text that will be shown for the third custom link on the side menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 3 URL** : Refers to the URL for the third custom link on the side menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage.

- **Link 4** : Refers to the text that will be shown for the fourth custom link on the side menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 4 URL** : Refers to the URL for the fourth custom link on the side menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage.

*sorry for the copy paste on these above*

- **Popup Title* : Refers to the title that will be shown on the About popup. Any text input is valid. The formatting of this title is the same as the main blog title.

- **Archive Tooltip** : Refers to the help text that will be shown when the cursor hovers the archive button on the side menu. Any text input is valid.

- **Home Tooltip** : Refers to the help text that will be shown when the cursor hovers the home button on the side menu. Any text input is valid.

- **Ask Tooltip** : Refers to the help text that will be shown when the cursor hovers the ask button on the side menu. Any text input is valid.

- **Search Tooltip** : Refers to the help text that will be shown when the cursor hovers the search button on the side menu. Any text input is valid.

- **Button n Icon** : Refers to the icons that will be shown on the buttons of the side menu. The icons are imported from [cappuccicons.com](https://cappuccicons.com/). Visit the previous link and search for the desired icons (use <kbd>Ctrl</kbd> + <kbd>F</kbd> to search by keywords.) For example, if the desired icon for button 5 is 'bee-o', copy and paste the name 'bee-o' on the input.

<figure>
    <img width="300" src="imgs/beeicon.png" />
    <figcaption>Border with big dots.</figcaption>
</figure>

<figure>
    <img width="300" src="imgs/beeicon2.png" />
    <figcaption>Bee icon on 5th button.</figcaption>
</figure>

### 🔷 Customizing About Popup

The following options are exclusive of the customization about section popup. If you don't have this option enabled, you don't have to fill/edit any of these options.

- **Window Text** : Refers to the content that will be displayed on the About Popup. It can have any length, the content will automatically scroll. The input can be simple raw text or include html tags. 

#### 💫 Tags 

The "Tags" tab will render a list of tumblr tags given by the user. If the *Tags List* text input is empty, this tab will not be shown.


- **Tags List** : Refers to a list of tags that will be rendered on the About Popup. If this has an input, a link "Tags" will be rendered on the navigation menu of the popup. To display the list correctly, needs to be written with the following format: 


```my tag 1, my tag 2, mytag3, another tag```

#### 💫 Links

To add a custom link to the navigation menu, just add a page to your theme and select the option "Link".

<figure>
    <img src="imgs/link.png" />
</figure>


--------

## 🌸 Other notes

- On the customization panel, the first time you install the theme maybe you would have to toggle the conditional switches on and off to get them to work properly (the customization panel is buggy sometimes).

--------
## 🌸 Terms of Use

- You can't remove credits (visible credits or in code credits).
- You can't copy portions of the code into any other works.
- You can't redistribute these works on other blogs or websites.
- You can't use the themes outside Tumblr or try to replicate them in other platform.
- You can use pages in other websites.
- You can customize anything (of the code) as you want.