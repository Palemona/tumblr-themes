# Glazed

A theme made for Tumblr blogs. Developed by Palemomos from [Themes By Pale (or Palemomos Themes)](https://themesbypale.tumblr.com/). 

## 🌸 Install this theme

### Install via Theme Garden

1. To install the theme from the Tumblr's Theme Garden, go to [this link](https://www.tumblr.com/theme/41219) and click on **"Install"**. 

2. A dropdown will appear with a list of your main and side blogs. Select the blog you want to use the theme, and click **"Install"** again.

Note: It is recommended to use Theme Garden to install the theme, because when new changes/fixes are added, the theme will be updated automatically.

### Install with source code

1. To install the theme using the source code, go to [this link](https://gitlab.com/Palemona/tumblr-themes/-/raw/master/Glazed/glazed.html), you'll be redirected to a page containing the source code.

2. Copy ALL the content of the page. Use <kbd>Ctrl</kbd> + <kbd>A</kbd> or <kbd>Command ⌘</kbd> + <kbd>A</kbd> to select all the text, and then copy it (<kbd>Ctrl</kbd> + <kbd>C</kbd> or <kbd>Command ⌘</kbd> + <kbd>C</kbd>).

3. Go to your blog and find the **Edit appereance** option. You'll be redirected you to the customization page.

4. Click on **Edit HTML**. Delete all the current content, and paste the code copied in step 2 <kbd>Ctrl</kbd> + <kbd>V</kbd> or <kbd>Command ⌘</kbd> + <kbd>V</kbd>.

5. Click **Update Preview** and then click **Save**.

--------

## 🌸 Customization Guide

The theme comes with the following options in the customization panel.

### 🔷 Images

- **Custom Avatar** : Refers to the image displayed in the sidebar. 

<figure>
    <img width="300" src="imgs/sidebar.png" />
</figure>


- **BG Image** : Refers to the image that will be used for the page background. To change the behaviour of the background, see *Background Repeat*.

- **About Section Cover** : Refers to the image that will be shown on the popup on the left side.

- **About Section Avatar** : Refers to the small image that will be shown on the popup on the right side (always will be rendered in size of 120px x 120px).

<figure>
    <img width="400" src="imgs/about.png" />
    <figcaption>Cover image on the left and Avatar image on the right</figcaption>
</figure>


### 🔷 Colors

- **Accent Color** : Refers to the color used for links and the first color for gradients (for example the blog's title). This color will be only used on Light Mode.

- **Accent Color Dark Mode** : Refers to the accent color used in Dark Mode, works exactly the same as *Accent Color*.

- **Accent Color 2**: Refers to the second color used in gradients and the background color used for the controls sections on each post. This color will automatically be "more transparent". This color will be used both on Dark Mode and Light Mode.

<figure>
    <img width="400" src="imgs/colors.png" />
    <figcaption>Accent color is blue and Accent color 2 is pink on Light Mode.</figcaption>
</figure>

<figure>
    <img width="400" src="imgs/colors2.png" />
    <figcaption>Accent color Dark Mode is green and Accent color 2 is pink on Dark Mode.</figcaption>
</figure>

### 🔷 Mandatory options

- **Font Style** : Refers to the font which will be used in all the theme except the blog's title and links. Possible values are listed on image 1.

- **Font Style Title** : Refers to the font which will be used only on the blog's title and links. Possible values are listed on image 1.

- **Font Style Links** : Refers to the font which will be used only on all the links of the blog (this includes links inside post captions). Possible values are listed on image 1.

<figure>
    <img width="400" src="imgs/fonts.png" />
    <figcaption>List of all possible values for fonts.</figcaption>
</figure>

- **Font Size** : Refers to the general size of the texts of the theme. The possible values are *12px*, *14px* and *16px*. Note that the size of the blog's title will be always bigger than the selected value and the size of the links of the post's controls is always smaller than the selected value (these are calculated depending on the selected size).

- **Columns** : Refers to the number columns used for posts. Possible values are *1* and *2*. Note that posts will be displayed in a grid behaviour:
 >first second
 >
 >thrid fourth
 >
 >fifth sixth.


### 🔷 Conditionals

- **Background Repeat** : Refers to the behaviour of the background image. If on, will render the background image repeated on x and y axes, and will not resize the original size of the image (this is good for patterns). If off, will expand the image to cover all the available space of the page.

- **Show Tags** : Refers if the tags (if have) of a post will be rendered on each post.

- **Hide captions** : Refers if the caption of the posts will be hidden in the Home Page. Note that caption will be always visible on Permalink Page. Also, captions from texts posts will not be hidden.

<div style="display: flex;">
    <figure text-align: center;">
        <img width="300" src="imgs/caption.png" />
        <figcaption>Hide captions not enabled</figcaption>
    </figure>
    <figure text-align: center;">
        <img width="300" src="imgs/nocaption.png" />
        <figcaption>Hide captions enabled</figcaption>
    </figure>
</div>

- **Show About Section** : Refers if the "About" section will be rendered or not. If active, will display an "About" link on the navigation menu. To read more about the customization of this section, checkout *Customizing About Section*.

- **Hide Loading Screen** : Refers if the animation screen when page is loading will be displayed or not.

- **Show Following** : Refers if the section displaying all the blogs the users follow will be displayed or not on the "About" section. To read more about the customization of this section, checkout *Customizing About Section*.

- **Drop Posts Animation** : Refers if the animation of the posts when the page is loading will be played or not.

### 🔷 Input Texts

- **Link 1** : Refers to the text that will be shown for the first custom link on the navigation menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 1 URL** : Refers to the URL for the first custom link on the navigation menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage. Example: *https://www.youtube.com/watch?v=dQw4w9WgXcQ* .

- **Link 2** : Refers to the text that will be shown for the second custom link on the navigation menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 2 URL** : Refers to the URL for the second custom link on the navigation menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage. 

- **Link 3** : Refers to the text that will be shown for the third custom link on the navigation menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 3 URL** : Refers to the URL for the third custom link on the navigation menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage.

- **Link 4** : Refers to the text that will be shown for the fourth custom link on the navigation menu. Any input is valid. If the input is empty, the link will be not rendered.

- **Link 4 URL** : Refers to the URL for the fourth custom link on the navigation menu. If the input is empty, when the user clicks on the link the page will not redirect to anything. This value needs to be a full valid direction of a webpage.

*sorry for the copy paste on these above*

### 🔷 Customizing About Section

The following options are exclusive of the customization about section popup. If you don't have this option enabled, you don't have to fill/edit any of these options. The About section has 4 tabs: **About**, **Tags**, **BlogRoll**, **Extra**.

#### 💫 About Tab

 The "About" tab will always be visible, and corresponds to the main information of the user. 

<figure>
    <img width="400" src="imgs/about.png" />
    <figcaption>Appearence of the "About" tab.</figcaption>
</figure>

The following options are optionals, remember you decide which personal information want to share.

- **About Name** : Refers to the name/username/nickname of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Age** : Refers to the age of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Zodiac** : Refers to the Zodiac Sign of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Location** : Refers to the current location or born place of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Occupation** : Refers to the current location or born place of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Pronouns** : Refers to the pronouns of the user. Any input is valid, but if empty, this line will be hidden automatically.

- **About Description** : Refers to an extra description/message of the user, can also be used to place a quote. Any input is valid, and the lenght of the text can be as desired, if it is too long the box will add a scrollbar automatically. If the input is empty, the box will not be rendered.

The following options refers to the "skills" of the user. User can set a maximum of 4 skills. All the 4 options works the same.

- **About Skill [ Number ] Label** : Refers to the legend of the skill.

- **About Skill [ Number ] Total** : Refers to the level of completness of the skill. Must be a number between 0 and 100.

<figure>
    <img width="200" src="imgs/skill.png" />
    <figcaption>Example of about skill with label "Drawing" and total 50.</figcaption>
</figure>

The following options are just links to the social networks of the user. This links will be displayed with icons of the corresponding social network. All these inputs need to be the full address of the profile/page of the user. 


- **Website URL** : Refers to the link of the webiste of the user. 

- **Twitter URL** : Refers to the link of the Twitter profile of the user. 

- **Instagram URL** : Refers to the link of the Instagram profile of the user. 

- **Shop URL** : Refers to the link of the online store (etsy, shopify, etc.) of the user.

- **Twitch URL** : Refers to the link of the Twitch page of the user. 

- **Facebook URL** : Refers to the link of the Facebook profile or page of the user. 

- **Spotify URL** : Refers to the link of the Spotify page/playlist of the user. 

#### 💫 Tags Tab

The "Tags" tab will render a list of tumblr tags given by the user. If the *Tags List* text input is empty, this tab will not be shown.

<figure>
    <img width="400" src="imgs/tags.png" />
    <figcaption>Appearence of the "Tags" tab.</figcaption>
</figure>

- **Tags List** : Refers to a list of tags that will be rendered on the "Tags" tab of the About Section. If this has an input, a link "Tags" will be rendered on the navigation menu of the popup. To display the list correctly, needs to be written with the following format: 

>["Section 1", "pixels", "pixel art", "kawaii", "landscapes", "photography", "art"],
>
>["Section 2", "nature", "green", "animals", "fantasy", "magic", "dogs"],
>
>["Section 3", "codes", "themes", "pages", "resources", "layouts", "html"]

You can separate your tags into subsections. Each section is a set of words wrapped by [ ], and each word must be wrapped by " " and separated by commas. The first word is always the section title. Also, each section must be separated by a comma, but note that the last section does not have a trailing comma. In the last example there are three subsections for tags, if you only want one subsection for your tags just copy and paste the following:

>["Section 1", "codes", "themes", "pages", "resources", "layouts", "html"]

**IMPORTANT NOTE** : Make sure you're always using the same double comma symbol. Some text editors may change the first opening double comma to “ or the last comma to ”. Additionally, you can replace the double comma (") by one single comma ('). 

#### 💫 BlogRoll Tab

 The "BlogRoll" tab will shown the list of the blogs the user follows. This option has to be enabled with the option *Show Following* on the conditionals options.

<figure>
    <img width="400" src="imgs/blogroll.png" />
    <figcaption>Appearence of the "BlogRoll" tab.</figcaption>
</figure>

#### 💫 Extra Tab

 The "Extra" tab will shown an "extra" content of the user. This can be a full detailed text with html tags. 

<figure>
    <img width="400" src="imgs/extra.png" />
    <figcaption>Appearence of the "Extra" tab.</figcaption>
</figure>

- **Extra Text** : Refers to the content that will be displayed on the "Extra" section of the About section. It can have any length, the content will automatically scroll. The input can be simple raw text or include html tags. If this input is empty, the "Extra" section will not be visible.

--------
## 🌸 Other notes

- If the user hovers a post with the cursor, the background will automatically be more opaque, this to mantain the aesthetic of "glassy" effect but also to have a better accessibility when reading the content of the post.
- The hide loading option was developed because of some firefox browsers appareantly never hided the loading screen when the pages finished loading. Changes in code were made and presumily this issue is fixed now, but anyways the user is freely to activate or deactivate this option. 
- Dark Mode and Light Mode are not an option in the customization panel, so the user who visits the blog can freely choose which option is better for them in terms of readability.
- On the customization panel, the first time you install the theme maybe you would have to toggle the conditional switches on and off to get them to work properly (the customization panel is buggy sometimes).
--------
## 🌸 Terms of Use

- You can't remove credits (visible credits or in code credits).
- You can't copy portions of the code into any other works.
- You can't redistribute these works on other blogs or websites.
- You can't use the themes outside Tumblr or try to replicate them in other platform.
- You can use pages in other websites.
- You can customize anything (of the code) as you want.