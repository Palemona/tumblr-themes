# Themes by Pale (or Palemomos Themes)

This is the official Gitlab repository for all the themes and pages made by Pale, from [Themes By Pale (or Palemomos Themes)](https://themesbypale.tumblr.com/). 


----


## ･ ｡ﾟ☆: *. How to install a theme

### 🌸 Install via Theme Garden

1. To install the theme from the Tumblr's Theme Garden, go to the corresponding page of the theme, and click on **"Install"**. Browse all Pale's themes in Theme Garden [here](https://www.tumblr.com/themes/by/themesbypale).

2. A dropdown will appear with a list of your main and side blogs. Select the blog you want to use the theme, and click **"Install"** again.

### 🌸 Install with source code

1. To install the theme using the source code, find the link of the source code of the the theme you want to install (checkout portfolio), you'll be redirected to a page containing the source code.

2. Copy ALL the content of the page. Use **CTRL+A** or **COMMAND+A** to select all the text, and then copy it (CTRL+C or COMMAND+C).

3. Go to your blog and find the **Edit appereance** option. You'll be redirected you to the customization page.

4. Click on **Edit HTML**. Delete all the current content, and paste the code copied in step 2 (CTRL+V or COMMAND+V).

5. Click **Update Preview** and then click **Save**.


-----


## ･ ｡ﾟ☆: *. Portfolio

### 🌸 Themes

<figure>
    <img src="assets/thumbs/backtotheclassics.png">
    <figcaption>Back to the classics | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/backtotheclassics.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/seethestarswithmeagain.jpg">
    <figcaption>See The Stars With Me, Again | <a href="https://www.tumblr.com/theme/41258">Theme Garden</a> | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/See_The_Stars_With_Me,_Again/seethestarswithmeagain.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/magicaldoremi.png">
    <figcaption>Magical DoReMi | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/magicaldoremi.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/socialmedia.png">
    <figcaption>Social Media | <a href="https://www.tumblr.com/theme/41171">Theme Garden</a> | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/Social_Media/socialmedia.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/doodling3x1.png">
    <figcaption>Doodling 3x1 | <a href="https://www.tumblr.com/theme/41199">Theme Garden</a> | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/Doodling3x1/doodling3x1.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/glazed.png">
    <figcaption>Glazed | <a href="https://www.tumblr.com/theme/41219">Theme Garden</a> | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/Glazed/glazed.html">Source Code</a></figcaption>
</figure>

### 🌸 Pages

<figure>
    <img src="assets/thumbs/gamelogpage.gif" width="375">
    <figcaption>Game Log | <a href="https://gitlab.com/Palemona/tumblr-themes/-/blob/master/game_log.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/crew.gif" width="375">
    <figcaption>Crew | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/crew_character_page/bleach_version.html">Source Code</a></figcaption>
</figure>

<figure>
    <img src="assets/thumbs/yugioh.gif" width="375" >
    <figcaption>Heart of Cards | <a href="https://gitlab.com/Palemona/tumblr-themes/-/raw/master/yugioh_cards_page/index.html">Source Code</a></figcaption>
</figure>

--------

## ･ ｡ﾟ☆: *. Terms of Use

- You can't remove credits (visible credits or in code credits).
- You can't copy portions of the code into any other works.
- You can't redistribute these works on other blogs or websites.
- You can't use the themes outside Tumblr or try to replicate them in other platform.
- You can use pages in other websites.
- You can customize anything (of the code) as you want.